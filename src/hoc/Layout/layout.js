
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Aux from '../Aux/Aux';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import CssBaseline from '@material-ui/core/CssBaseline';  
import * as action from '../../store/actions/index';
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem';
import Link from '@material-ui/core/Link';
import Copyright from '../../components/UI/Copyright/Copyright';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';



const drawerWidth = 250;
const useStyles = (theme) => ({
  root: {
        flexgrow: 1,
        display: 'flex'
    },
  appBar: {
      zIndex: theme.zIndex.drawer + 1,
      marginBottom: theme.spacing(2)
  },
menuButton: {
      marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
        display: 'none'
      }
    },
    title: {
        flexGrow: 1,
  },
  navbar: {
    [theme.breakpoints.down('md')]: {
        display: 'none'
      }
    },  
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    paddingTop: theme.spacing(4)

  },
  categoryDrawer: {
    [theme.breakpoints.down('md')]: {
        display: 'none'
      },
       width: 200,
       flexShrink: 0,
   },
  drawerPaper: {
      width: 200,
    paddingTop: theme.spacing(4),
    textColor: 'blue'
  },
    drawerContainer: {
     display: 'block',
    justifyContent: 'center',
    textAlign: 'left',
    marginTop: "80%",
    // padding: "2%",
       overflow: 'auto',
   },
  content: {
    margin: "5%",
    marginTop:"10%",
    width: '80%',
      padding: theme.spacing(3),
      minHeight: '100vh'

  },
  link: {
    display: 'block',
        justifyContent: 'center',
        textAlign: 'left',
        margin: "4%",
        padding: "2%",

    },
    footer: {
        marginTop: "5%",
        color: 'primary',
        textDecoration: 'black',
        textAlign: 'center',
      
  }
});




class Layout extends Component {
  state = {
    open: false,
    anchorEl: null,
  }
  componentDidMount() {
    this.props.onFetchCategories();

  }

  toggleDrawerHandler = (event) => {
     if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
         return;
     }
    this.setState((prevState) => {
        return {
            open: !prevState.open
        };
    });
  }
  openMenuHandler = (event) => {
    this.setState({ anchorEl: event.currentTarget   });
  }
  closeMenuHandler = (event, slug=null) => {
    this.setState({
      anchorEl: null,
    });
  }

  render() {
    
      const { classes } = this.props;
      let buttons = (
        <Aux>
          <Button href="/" color="inherit">Home</Button>
          <Button href='/addarticle' color="inherit">Add Article</Button>
        <Button href="/signin" color="inherit">Sign In</Button>
          <Button href="/signup" color='inherit'>Sign Up</Button>
          <Button href="/contactus" color='inherit'>Contact Us</Button>
        </Aux>
      );
      if (this.props.isAuthenticated) {
        buttons = ( 
          <Aux>
            <Button href="/" color="inherit">Home</Button>
            <Button href='/addarticle' color="inherit">Add Article</Button>
            <Button href="/dashboard" color='inherit'>Dashboard</Button>
            <Button href="/contactus" color='inherit'>Contact Us</Button>
          <Button href="/logout" color="inherit">Logout</Button>
          </Aux>
        );
      }
    
    if (this.props.isAdmin) {
      buttons = ( 
          <Aux>
            <Button href="/" color="inherit">Home</Button>
            <Button href='/addarticle' color="inherit">Add Article</Button>
          <Button href="/dashboard" color='inherit'>Dashboard</Button>
          <Button href="/admindashboard" color='inherit'>Admin Dashboard</Button>
          <Button href="/contactus" color='inherit'>Contact Us</Button>
          <Button href="/logout" color="inherit">Logout</Button>
          </Aux>
        );
        
      }

        return (
          <Aux className={classes.root}>
            <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
                <IconButton edge="start" className={classes.menuButton} onClick={(event) => this.toggleDrawerHandler(event)} color="inherit" aria-label="menu">
                  <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
                  Article Zone
                </Typography> 
                <Typography variant="h6" className={classes.navbar}>
                  {buttons}
                  
                </Typography>           
              </Toolbar>
              <Drawer open={this.state.open} onClose={this.toggleDrawerHandler} className={classes.drawer} classes={{
          paper: classes.drawerPaper,
              }}>
                  {buttons}
                
              </Drawer>
       
            </AppBar>
            <Drawer
                anchor='right'
                className={classes.categoryDrawer}
                variant="permanent"
                 classes={{
                  paper: classes.drawerPaper,
                }}>
                    <div className={classes.drawerContainer}>
                        <Toolbar>
                            <Typography variant="h6" className={classes.title}>
                    Categories
                </Typography>   
                        </Toolbar>
                      <Divider />
                    {
                      this.props.categories.map((category) => (
                        category.articleCount > 0 ? 
                          <Aux key={category.identifier}>
                            <Link className={classes.link} key={category.identifier} 
                            href={"/articles/category/" + category.slug}
                            value={category.slug}
                            color="primary"
                            variant="body2" > 
                          {category.title+"("+category.articleCount+")"}
                          </Link><Divider /></Aux> : null
                      ))
                    }
                </div>

                </Drawer>
                <Paper elevation={1} className={classes.content} m={2}>
                <main >
                    {this.props.children}
                    </main>
                    </Paper>
      
                 <footer className={classes.footer}>
                    <Container maxWidth="sm">
                         <Typography variant="body1">ArticleZone is a  free blogging website </Typography>
                         <Copyright />
                     </Container>
            </footer>
    </Aux>

        );
    }
}
  
const mapStateToProps = state => {
    return {
      categories: state.categories.categories,
      isAuthenticated: state.auth.token !== null,
      isAdmin: state.userDetails.userDetails.isAdmin
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCategories: () => (dispatch(action.fetchCategories()))
    }
}
export default  connect(mapStateToProps,mapDispatchToProps)((withStyles(useStyles)(Layout)));

