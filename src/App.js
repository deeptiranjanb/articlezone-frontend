import React, { Component } from 'react';
import { Route, Switch, withRouter,Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/layout';
import Articles from './containers/Articles/Articles';
import SignIn from './containers/auth/SignIn';
import SignUp from './containers/auth/SignUp';
import FullArticle from './containers/FullArticle/FullArticle';
import NewArticle from './containers/NewArticle/NewArticle';
import CategoryArticles from './containers/CategoryArticles/CategoryArticles';
import UserDashboard from './containers/UserDashboard/UserDashboard';
import AdminDashboard from './containers/AdminDashboard/AdminDashboard';
import UpdateArticle from './containers/UpdateArticle/UpdateArticle';
import UpdateComment from './containers/UpdateComment/UpdateComment';
import DeleteArticle from './containers/DeleteArticle/DeleteArticle';
import DeleteComment from './containers/DeleteComment/DeleteComment';
import ContactUs from './containers/ContactUs/ContactUs';
import UpdateUser from './containers/UpdateUser/UpdateUser';
import DeleteUser from './containers/DeleteUser/DeleteUser';
import Logout from './containers/auth/Logout';
import * as actions from './store/actions/index';
import classes from './App.css';


class App extends Component {
  componentDidMount() {
  this.props.onTryAutoSignup();
  }
  render() {
    let routes = (
      <Switch>
        <Route path="/contactus" component={ContactUs} />
        <Route path="/signup" component={SignUp} />
        <Route path="/signin" component={SignIn} />
        <Route path="/addarticle" component={NewArticle} />
        <Route path="/articles/category/:slug" component={CategoryArticles} />
        <Route path="/article/:slug" component={FullArticle} />
        <Route path="/" component={Articles} />
        <Redirect to="/" />
      </Switch>
    );
    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path="/updateusers/:id" component={UpdateUser} />
          <Route path='/updatecomments/:id' component={UpdateComment} />
          <Route path='/deletecomments/:id' component={DeleteComment} />
          <Route path="/contactus" component={ContactUs} />
          <Route path="/addarticle" component={NewArticle} />
          <Route path="/deletearticles/:id" component={DeleteArticle} />
          <Route path="/updatearticles/:id" component={UpdateArticle} />
          <Route path="/dashboard"  component={UserDashboard} />
          <Route path="/articles/category/:slug" component={CategoryArticles} />
          <Route path="/article/:slug" component={FullArticle} />
          <Route path="/logout"  component={Logout} />
          <Route path="/" component={Articles} />
          <Redirect to="/" />
        </Switch>
      )
    }
    if (this.props.isAdmin) {
      routes = (
        <Switch>
          <Route path="/updateusers/:id" component={UpdateUser} />
          <Route path="/deleteusers/:id" component={DeleteUser} />
          <Route path='/updatecomments/:id' component={UpdateComment} />
          <Route path='/deletecomments/:id' component={DeleteComment} />
          <Route path="/contactus" component={ContactUs} />
          <Route path="/dashboard" component={UserDashboard} />
          <Route path="/addarticle" component={NewArticle} />
          <Route path="/deletearticles/:id" component={DeleteArticle} />
          <Route path="/updatearticles/:id" component={UpdateArticle} />
          <Route path="/admindashboard" component={AdminDashboard} />
          <Route path="/articles/category/:slug" component={CategoryArticles} />
          <Route path="/article/:slug" component={FullArticle} />
          <Route path="/logout"  component={Logout} />
          <Route path="/" component={Articles} />
          <Redirect to="/" />
        </Switch>

      )
    }

    return ( 
      <div className={classes.App}>
        <Layout> 
          { routes }
        </Layout>
        </div>
    );
  }
}
const mapStateToProps = state => {
    return {
      isAuthenticated: state.auth.token !== null,
      isAdmin : state.userDetails.userDetails.isAdmin
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

