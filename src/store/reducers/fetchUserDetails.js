import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    userDetails: {
    id: null,
    name: null,
    email: null,
    mobileNo: null,
    isAdmin: null,
    isVerified: null,
    },
    error: null,
    loading: false
}

const fetchUserDetailsStart = (state, action) => {
    return updateObject(state, { loading: false });
}

const fetchUserDetailsSuccess = (state, action) => {
    return updateObject(state, {
        userDetails: {
        id: action.data.data.identifier,
        name: action.data.data.name,
        email: action.data.data.email,
        mobileNo: action.data.data.mobileNo,
        isVerified: action.data.data.isVerified,
        isAdmin: action.data.data.isAdmin
        },
        loading: false,
        error: null
    });
}

const fetchUserDetailsFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    })
}
const logout = (state, action) => {
    localStorage.clear();
    return updateObject(state,initialState);
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_DETAILS_START: return fetchUserDetailsStart(state, action);
        case actionTypes.FETCH_USER_DETAILS_SUCCESS: return fetchUserDetailsSuccess(state, action);
        case actionTypes.FETCH_USER_DETAILS_FAIL: return fetchUserDetailsFail(state, action);
        case actionTypes.USER_LOGOUT: return logout(state,action);
        default: return state;
    }
}

export default reducer;