import * as actionTypes from '../actions/actionTypes';
import { updateObject,updateObjectInArray } from '../utility';


const initialState = {
    articles: [],
    totalPage: 1,
    totalArticles: 0,
    loading: false,
    error: null
}

const fetchUserArticlesStart = (state, action) => {
    return updateObject(state, {
        loading: true
    });
}

const fetchUserArticlesSuccess = (state, action) => {

    const updatedArticles = updateObjectInArray(state.articles, action.articles.data);
    const updatedState = {
        articles: updatedArticles,
        totalPage: action.articles.meta.pagination.total_pages,
        totalArticles: action.articles.meta.pagination.total,
        loading: false
    }
    return updateObject(state, updatedState);
}

const fetchUserArticlesFailed = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_ARTICLES_START:
            return fetchUserArticlesStart(state, action);
        case actionTypes.FETCH_USER_ARTICLES_SUCCESS:
            return fetchUserArticlesSuccess(state, action);
        case actionTypes.FETCH_USER_ARTICLES_FAIL:
            return fetchUserArticlesFailed(state, action);
        default:
            return state;
    }
}

export default reducer;