import * as actionTypes from '../actions/actionTypes';
import { updateObject,updateObjectInArray } from '../utility';


const initialState = {
    articles: [],
    totalPages: 1,
    totalArticles:1,
    loading: false,
    error: null
}

const fetchArticlesStart = (state, action) => {
    return updateObject(state, { loading: true });
}

const fetchArticlesSuccess = (state, action) => {
    const updatedArticles = updateObjectInArray(state.articles, action.articles.data);
    console.log(updatedArticles);
    const updatedState = {
        articles: action.articles.data,
        totalPages: action.articles.meta.pagination.total_pages,
        totalArticles: action.articles.meta.pagination.total,
        loading: false
    }

    return updateObject(state,updatedState);
    
}

const fetchArticlesFailed = (state, action) => {
    return updateObject(state, { error:action.error, loading: false });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ARTICLES_START: return fetchArticlesStart(state,action);
        case actionTypes.FETCH_ARTICLES_SUCCESS: return fetchArticlesSuccess(state, action);
        case actionTypes.FETCH_ARTICLES_FAIL: return fetchArticlesFailed(state, action);
        default: return state;
    }
}

export default reducer;