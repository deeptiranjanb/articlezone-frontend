import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    userId: null,
    name: null,
    email: null,
    mobileNo: null,
    loading: false,
    error: null
}

const signupStart = (state, action) => {
   return updateObject(state, { loading: true });
}

const signupSuccess = (state, action) => {
    console.log(state);
    return updateObject(state, { 
        userId: action.userId,
        name: action.name,
        email: action.email,
        mobileNo: action.mobileNo,
        loading: false
     });
}

const signupFail = (state, action) => {
   return updateObject(state, {
        loading: false,
        error: action.error
    })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SIGNUP_START: return signupStart(state, action);
        case actionTypes.SIGNUP_SUCCESS: return signupSuccess(state, action);
        case actionTypes.SIGNUP_FAIL: return signupFail(state, action);
        default: return state;
    }
}

export default reducer;