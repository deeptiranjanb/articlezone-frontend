import * as actionTypes from '../actions/actionTypes';
import { updateObject, updateObjectInArray } from '../utility';


const initialState = {
    categories: [],
    meta: [],
    loading: false,
    error: null
}

const fetchCategoriesStart = (state, action) => {
    return updateObject(state, { loading: true });
}

const fetchCategoriesSuccess = (state, action) => {
    const updateCategories = updateObjectInArray(state.categories, action.categories.data);
    const updatedState = {
        categories: updateCategories,
        meta: action.categories.meta.pagination,
        loading: false
    }
    console.log(updatedState);
    return updateObject(state,updatedState);
}

const fetchCategoriesFailed = (state, action) => {
    return updateObject(state, { error:action.error, loading: false });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ARTICLE_CATEGORIES_START: return fetchCategoriesStart(state,action);
        case actionTypes.FETCH_ARTICLE_CATEGORIES_SUCCESS: return fetchCategoriesSuccess(state, action);
        case actionTypes.FETCH_ARTICLE_CATEGORIES_FAIL: return fetchCategoriesFailed(state, action);
        default: return state;
    }
}

export default reducer;