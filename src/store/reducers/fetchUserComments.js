import * as actionTypes from '../actions/actionTypes';
import { updateObject,updateObjectInArray } from '../utility';


const initialState = {
    comments: [],
    totalPages: 1,
    totalComments: 0,
    loading: false,
    error: null
}

const fetchUserCommentsStart = (state, action) => {
    return updateObject(state, {
        loading: true
    });
}

const fetchUserCommentsSuccess = (state, action) => {
    if (action.comments.data.length > 0) {
    const updatedComments = updateObjectInArray(state.comments, action.comments.data);
        const updatedState = {
            comments: updatedComments,
            totalPages: action.comments.meta.pagination.total_pages,
            totalComments: action.comments.meta.pagination.total,
            loading: false
        }
        return updateObject(state, updatedState);
    }
    return updateObject(state, initialState);
}

const fetchUserCommentsFailed = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_COMMENTS_START:
            return fetchUserCommentsStart(state, action);
        case actionTypes.FETCH_USER_COMMENTS_SUCCESS:
            return fetchUserCommentsSuccess(state, action);
        case actionTypes.FETCH_USER_COMMENTS_FAIL:
            return fetchUserCommentsFailed(state, action);
    
        default:
            return state;
    }
}

export default reducer;