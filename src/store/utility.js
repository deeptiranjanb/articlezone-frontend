export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const updateObjectInArray = (array, action) => {
    if (array.length === 0) {
        return action;
    }
   
    const newArray = array.map((item, index) => {
        if (action[index]) {
            return {
                ...item,
                ...action[index]
            }
        }
        return null;

        // Otherwise, this is the one we want - return an updated value
        
    });
    var filtered = newArray.filter(function (el) {
        return el != null;
    })
    return filtered;
}