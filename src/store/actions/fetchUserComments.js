import * as actionTypes from './actionTypes';
import axios from '../../axios-custom';

export const fetchUserCommentsStart = () => {
    return {
        type: actionTypes.FETCH_USER_COMMENTS_START
    }
}

export const fetchUserCommentsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_USER_COMMENTS_SUCCESS,
        comments: data
    }
}

export const fetchUserCommentsFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_COMMENTS_FAIL,
        error: error
    }
}

export const fetchUserComments = () => {
    return dispatch => {
        dispatch(fetchUserCommentsStart());
        const id = localStorage.getItem('id');
        const token = localStorage.getItem('token');
        const tokenType = localStorage.getItem('tokenType');
        axios.get('/user/' + id + '/comments', {
            headers: {
                Authorization: tokenType + ' ' + token
            }
        })
            .then(response => {
                dispatch(fetchUserCommentsSuccess(response.data));

            }).catch(err => {
                console.log(err);
                dispatch(fetchUserCommentsFail(err.response));
            });
    }
    
}