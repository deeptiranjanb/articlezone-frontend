import * as actionTypes from './actionTypes';
import * as actions from './index';
import axios from '../../axios-custom';

export const signupStart = () => {
    return {
        type: actionTypes.SIGNUP_START
    }
}

export const signupSuccess = (data) => {
    return {
        type: actionTypes.SIGNUP_SUCCESS,
        userdata: data
    }
}

export const signupFail = (data) => {
    return {
        type: actionTypes.SIGNUP_FAIL,
        error: data
    }
}

export const signup = (data) => {
    return dispatch => {
        dispatch(signupStart());
        const userData = {
            name: data.firstName.value + ' ' + data.lastName.value,
            email: data.email.value,
            mobileNo: data.mobileNo.value,
            password: data.password.value,
            password_confirmation: data.confirmPassword.value

        }
        axios.post('/users', userData)
            .then(response => {
                dispatch(signupSuccess(response.data));
                dispatch(actions.auth(userData.email, userData.password));
            })
            .catch(error => {
                dispatch(signupFail(error.response));
            })

    }
}