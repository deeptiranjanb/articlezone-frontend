import * as actionTypes from './actionTypes';
import axios from '../../axios-custom';

export const fetchArticlesStart = () => {
    return {
        type: actionTypes.FETCH_ARTICLES_START
    }
}

export const fetchArticlesSuccess = (articles) => {
    return {
        type: actionTypes.FETCH_ARTICLES_SUCCESS,
        articles: articles
    }

}

export const fetchArticlesFailed = (error) => {
    return {
        type: actionTypes.FETCH_ARTICLES_FAIL,
        error: error
    }
}

export const fetchArticles = () => {
    return dispatch => {
        dispatch(fetchArticlesStart());
        axios.get('/articles?approvalStatus=1')
            .then(response => {
                dispatch(fetchArticlesSuccess(response.data));
            })
            .catch(error => {
               console.log(error);
               dispatch(fetchArticlesFailed(error));
            })
    }
}


export const fetchNextPage = (url) => {
    return dispatch => {
        console.log(url);
        dispatch(fetchArticlesStart());
        axios.get(url)
            .then(response => {
                console.log(response);
            dispatch(fetchArticlesSuccess(response.data))
            })
            .catch(error => {
                console.log(error);
            dispatch(fetchArticlesFailed(error))
        })

    }
}

export const fetchCategoryArticles = (url) => {
    return dispatch => {
        dispatch(fetchArticlesStart());
        console.log(url);
        axios.get(url)
            .then(response => {
                dispatch(fetchArticlesSuccess(response.data))
            })
            .catch(error => {
                dispatch(fetchArticlesFailed(error))
            })

    }
}
