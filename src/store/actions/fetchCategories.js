import * as actionTypes from './actionTypes';
import axios from '../../axios-custom';

export const fetchCategoriesStart = () => {
    return {
        type: actionTypes.FETCH_ARTICLE_CATEGORIES_START
    }
}

export const fetchCategoriesSuccess = (categories) => {
    return {
        type: actionTypes.FETCH_ARTICLE_CATEGORIES_SUCCESS,
        categories: categories
    }

}

export const fetchCategoriesFailed = (error) => {
    return {
        type: actionTypes.FETCH_ARTICLE_CATEGORIES_FAIL,
        error: error
    }
}

export const fetchCategories = () => {
    return dispatch => {
        dispatch(fetchCategoriesStart());
        axios.get('/categories?per_page=20')
            .then(response => {
                dispatch(fetchCategoriesSuccess(response.data));
            })
            .catch(error => {
                console.log(error);
                dispatch(fetchCategoriesFailed(error));
            })
    }
}