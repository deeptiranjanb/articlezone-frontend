import * as actionTypes from './actionTypes';
import axios from '../../axios-custom.js';
import * as actions from './index';
export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, tokenType) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        tokenType: tokenType
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000)

    };

}

export const logout = () => { 
    localStorage.clear();
        return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const auth = (email, password) => {
    return dispatch => {
        dispatch(authStart());

        const authData = {
            username: email,
            password: password,
            client_id: 2,
            client_secret: '4gRWyF7rLlVldy1feKG16DRL8OJjzX8O4mIXvV50',
            grant_type:'password'  
        }
        let url = '/oauth/token';
        axios.post(url, authData)
            .then(response => {
                console.log(response);
                const expirationDate = new Date(new Date().getTime() + response.data.expires_in * 1000)
                localStorage.setItem('token', response.data.access_token);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('tokenType', response.data.token_type);
                dispatch(authSuccess(response.data.access_token, response.data.token_type));
                dispatch(actions.fetchUserDetails(response.data.access_token, response.data.token_type));
                dispatch(checkAuthTimeout(response.data.expires_in));
            })
            .catch(error => {
                console.log(error);
                dispatch(authFail(error));
            });
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate < new Date()) {
                dispatch(logout());
                dispatch(actions.userLogout());
            } else {
                const tokenType = localStorage.getItem('tokenType');
                dispatch(authSuccess(token, tokenType));
                dispatch(actions.fetchUserDetails(token, tokenType));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}