import * as actionTypes from './actionTypes';
import axios from '../../axios-custom.js';

export const fetchUserDetailsStart = () => {
    return {
        type: actionTypes.FETCH_USER_DETAILS_START
    }
}

export const fetchUserDetailsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_USER_DETAILS_SUCCESS,
        data: data
    }
}

export const fetchUserDetailsFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_DETAILS_FAIL,
        error: error
    }
}
export const userLogout = () => {
    console.log('user logout actoin called');
    localStorage.clear();
    return {
        type: actionTypes.USER_LOGOUT
    }
}


export const fetchUserDetails = (token, tokenType) => {
    return dispatch => {
        dispatch(fetchUserDetailsStart);
        axios.get('/users/me', {
            headers: {
                Authorization:tokenType + ' '+ token
            }
        }).then(response => {
                console.log(response);
                localStorage.setItem('id', response.data.data.identifier);
              dispatch(fetchUserDetailsSuccess(response.data));
            })
            .catch(error => {
                console.log(error.response);
                dispatch(fetchUserDetailsFail(error.data));
            })
    }
}