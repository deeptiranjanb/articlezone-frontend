import * as actionTypes from './actionTypes';
import axios from '../../axios-custom';

export const fetchUserArticlesStart = () => {
    return {
        type: actionTypes.FETCH_USER_ARTICLES_START
    }
}

export const fetchUserArticlesSuccess = (data) => {
    return {
        type: actionTypes.FETCH_USER_ARTICLES_SUCCESS,
        articles:data
    }
}

export const fetchUserArticlesFail = (error) => {
    return {
        type: actionTypes.FETCH_USER_ARTICLES_FAIL,
        error: error
    }
}

export const fetchUserArticles = (id) => {
    return dispatch => {
        dispatch(fetchUserArticlesStart());
         const id = localStorage.getItem('id');
        const token = localStorage.getItem('token');
        const tokenType = localStorage.getItem('tokenType');
         axios.get('/user/' + id + '/articles', {
                 headers: {
                     Authorization: tokenType + ' ' + token
                 }
             })
             .then(response => {
                dispatch(fetchUserArticlesSuccess(response.data));
             }).catch(err => {
                 console.log(err.response);
                 dispatch(fetchUserArticlesFail(err));
             });
    }
}