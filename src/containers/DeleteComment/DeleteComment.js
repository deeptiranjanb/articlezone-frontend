import React, { Component } from 'react';
import { connect } from 'react-redux';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom';
import Spinner from '../../components/UI/Spinner/Spinner';

class DeleteComment extends Component {
    componentDidMount() {
        const url = '/comments/' + this.props.match.params.id;
        let formData = new FormData();
        formData.append('_method', 'DELETE');
        axios.post(url, formData, {
            headers: {
                'content-type': 'multipart/form-data',
                Authorization: this.props.tokenType+' '+ this.props.token
            }
        }).then(response => {
            this.props.history.goBack();
        }).catch(error => console.log(error.response));
    }
    render() {
        return (<Spinner />);
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        tokenType: state.auth.tokenType
    }
}

export default connect(mapStateToProps)(withErrorHandler(DeleteComment,axios));
