import React, { Component } from 'react';
import axios from '../../axios-custom';
import { withStyles } from '@material-ui/core/styles';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Alert from '@material-ui/lab/Alert';

import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';





const useStyles = (theme) => ({
  root: {
    '& .MuiTextField-root': {
          margin: theme.spacing(1),
            width: '100%'
        },
        
    },
    ckEditor: {
        marginLeft: theme.spacing(1),   
    },
     submit: {
         margin: theme.spacing(3, 0, 2),
    },
    formControl: {
         width: "100%"
     }
});


class UpdateComment extends Component {
   state = {
        inputs: {
            commentContent: {
                value: '',
                validation: {
                    required: true,
                    minLength: 20,
                },
                valid: true,
                touched: false
            },
       },
        loading: false,
         error: null, 
         redirect: null
   }
    
    componentDidMount() {
        const url = '/comments/' + this.props.match.params.id;
        axios.get(url, {
            headers: {
                Authorization: this.props.tokenType +' '+ this.props.token
            }
        })
            .then(response => {
                console.log(response);
                this.setState({
                    inputs: {
                        ...this.state.inputs,
                        commentContent: {
                            ...this.state.inputs.commentContent,
                           value: response.data.data.commentContent,
                        }

                    },
                    status: false
                })
            })
            .catch(error => console.log(error.response));
    }

    checkValidity(value, rules) {
        let isValid = true;
       
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        return isValid;
    }
    ckeditorInputChangeHandler = (event, editor) => {
           const updatedInputs = {
               ...this.state.inputs,
               commentContent: {
                   ...this.state.inputs.commentContent,
                   value: editor.getData(),
                  valid: this.checkValidity(editor.getData(),this.state.inputs.commentContent.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
checkboxHandler = (event) => {
    this.setState({
        status: event.target.checked
    })
}
    submitHandler = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        let formData = new FormData();
        if (this.state.inputs.commentContent.touched) {
            console.log(this.state.inputs.commentContent.value);
            formData.append('commentContent', this.state.inputs.commentContent.value);
        }
        if (this.state.status) {
            formData.append('isApproved', 'true');
        }
        formData.append('_method', 'put');
        const url = '/comments/' + this.props.match.params.id;
        axios.post(url, formData, {
                headers: {
                'content-type': 'multipart/form-data',
                Authorization: this.props.tokenType+ ' ' + this.props.token
                }
            })
            .then(res => {
                console.log(res);
                this.setState({ loading: false });
                this.props.history.goBack();
                
            })
            .catch(err => {
                this.setState({ loading: false });
                console.log(err)
            })
        };
    
    render() {
        const { classes } = this.props;
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        if (this.state.loading) {
            return <Spinner />;
        }

          let approve = null;
        if (this.props.isAdmin) {
                approve = (<FormControlLabel
                    control={<Checkbox checked={this.state.status} onChange={this.checkboxHandler} name="approve" />}
                    label="Approve Comment"
                    color="primary"
                />)
        }
        return (
            
            <div className={classes.root}>
                <form onSubmit={this.submitHandler}>
                    <Grid container spacing={2}>
                         <Grid item xs={0} md={4}>
                        </Grid>
                        
                        <Grid item xs={12} md={4} className={classes.ckEditor}>
                            <CKEditor
                                editor={ClassicEditor}
                                onInit={editor => {
                                }}
                                onChange={(event, editor) => this.ckeditorInputChangeHandler(event, editor)}
                                onBeforeLoad={(CKEDITOR) => (CKEDITOR.disableAutoInline = true)}
                                data={this.state.inputs.commentContent.value}
                                
                            />
                            {this.state.inputs.commentContent.valid?null:<Alert severity="error">Comment is requied</Alert>}
                            {approve}
                            <Button
                            type="submit"
                            variant="contained"
                            disabled={!this.state.inputs.commentContent.valid}
                            color="primary"
                            className={classes.submit}>
                                Submit
                        </Button>
                        </Grid>
                    </Grid> 
                    </form>
            </div>
        );
    }

  
}
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        tokenType: state.auth.tokenType,
        isAdmin: state.userDetails.userDetails.isAdmin,
        id: state.userDetails.userDetails.id
    }
}

export default connect(mapStateToProps)(withStyles(useStyles)(withErrorHandler(UpdateComment,axios)));