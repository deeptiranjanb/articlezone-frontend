import React, { Component } from 'react';
import axios from '../../axios-custom';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Alert from '@material-ui/lab/Alert';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';





const useStyles = (theme) => ({
    root: {
        '& .MuiTextField-root': {
            width: "100%"
        },
        margin: "5%"

    },
    ckEditor: {
         marginTop: "2%",
             marginBottom: "2%",
             width: "70%"
        
    },
    submit: {
        marginTop: "2%",
            marginBottom: "2%",
            width: "100%",

    },
    formControl: {
marginTop: "2%",
        marginBottom: "2%",
        width: "100%",    }
});


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};



class UpdateArticle extends Component {
   state = {
        inputs: {
            title: {
                value: '',
                validation: {
                    required: true,
                    minLength: 10,
                    maxLength: 100
                },
                valid: true,
                touched: false
            },
            excerpt: {
                value: '',
                validation: {
                    required: true,
                    minLength: 50,
                },
                valid: true,
                touched: false
            },
            content: {
                value: '',
                validation: {
                    required: true,
                    minLength: 100,
                },
                valid: true,
                touched: false
            },
            image: {
                value: null,
                validation: {
                        mimeType: ['image/jpg','image/jpeg','image/png'],
                        maxSize: 40000000
                    },
                    valid: true,
                touched: false
            },
            categories: {
                value: [],
                validation: {
                        array: true
                    },
                valid: true,
                touched: false
            },
         },
         loading: false, 
       redirect: null,
         status: false
   }
    
    componentDidMount() {
        const url = '/articles/' + this.props.match.params.id;
        axios.get(url, {
            headers: {
                Authorization: this.props.tokenType+' '+ this.props.token
            }
        })
            .then(response => {
                console.log(response);
                const newCategories = response.data.categories.map((category) => {
                    return category.identifier;
                })
                this.setState({
                    inputs: {
                        ...this.state.inputs,
                        title: {
                            ...this.state.inputs.title,
                            value: response.data.article.title
                        },
                        excerpt: {
                            ...this.state.inputs.excerpt,
                            value: response.data.article.excerpt,
                        },
                        content: {
                            ...this.state.inputs.content,
                            value: response.data.article.content,
                        },
                        categories: {
                            ...this.state.inputs.categories,
                            value: newCategories
                        },

                    },
                    status: response.data.article.approvalStatus
                })
            })
            .catch(error => console.log(error.response));
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (rules.array) {
            isValid = value.length >= 1 && isValid;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.minLength) {
            isValid = value.trim().length >= rules.minLength && isValid;
        }
        if (rules.maxLength) {
            isValid = value.trim().length <= rules.maxLength && isValid;
        }
        if (rules.mimeType) {
            isValid = rules.mimeType.indexOf(value.type) !== -1;

        }
        if (rules.maxSize) {
            isValid = value.size <= rules.maxSize && isValid;
        }
        return isValid;
    }
      inputChangedHandler = (event, name) => {
          const updatedInputs = {
              ...this.state.inputs,
              [name]: {
                  ...this.state.inputs[name],
                  value: event.target.value,
                  valid: this.checkValidity(event.target.value,this.state.inputs[name].validation),
                  touched: true
              }
          };
          this.setState({
              inputs: updatedInputs
          })
      }
    ckeditorInputChangeHandler = (event, editor) => {
           const updatedInputs = {
               ...this.state.inputs,
               content: {
                   ...this.state.inputs.content,
                   value: editor.getData(),
                    valid: this.checkValidity(editor.getData(),this.state.inputs.content.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    imageChangeHandler = (event) => {
        const file = event.target.files[0];
        const updatedInputs = {
               ...this.state.inputs,
               image: {
                   ...this.state.inputs.image,
                   value: file,
                    valid: this.checkValidity(file,this.state.inputs.image.validation),

                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    categoryChangeHandler = (event) => {
        const newCategories = event.target.value;
       const uniqueCategories = newCategories.filter(function (item, pos) {
            return newCategories.indexOf(item) === pos;
        })

        const updatedInputs = {
               ...this.state.inputs,
               categories: {
                   ...this.state.inputs.categories,
                   value: uniqueCategories,
                    valid: this.checkValidity(newCategories,this.state.inputs.categories.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    chipDeleteHandler = (event) => {
        let newCategories = [...this.state.inputs.categories];
        newCategories = newCategories.splice(event.target.value, 1);
        const updatedInputs = {
            ...this.state.inputs,
            categories: {
                ...this.state.inputs.categories,
                value: newCategories,
                touched: true
            }
        };
        this.setState({
            inputs: updatedInputs
        })
    }
    categoryNameSearchHandler = (id) => {
        let result;
        if (this.props.categories) {
            result = this.props.categories.filter(function (category) {
                return category.identifier === id;
            })
            if (result.length > 0) {
                return result[0].title;
            }
        }
        return id;
    }
    checkboxHandler = (event) => {
        this.setState({
            status: event.target.checked
        })
    }
    submitHandler = (event) => {
        event.preventDefault();
        this.setState({ loading: true });
        let formData = new FormData();
        if (this.state.inputs.title.touched) {
            formData.append('title', this.state.inputs.title.value);
        }
        if (this.state.inputs.excerpt.touched) {

            formData.append('excerpt', this.state.inputs.excerpt.value);
        }
        if (this.state.inputs.content.touched) {
            formData.append('content', this.state.inputs.content.value);
        }
        if (this.state.inputs.image.touched) {
            formData.append('image', this.state.inputs.image.value);
        }
        if (this.state.inputs.categories.touched) {
            formData.append('categories', this.state.inputs.categories.value);
        }
        if (this.state.status) {
            formData.append('approvalStatus', 'true');
        }
        formData.append('_method', 'put');
        const url = '/articles/' + this.props.match.params.id;
        axios.post(url, formData, {
                headers: {
                'content-type': 'multipart/form-data',
                Authorization: this.props.tokenType + ' ' + this.props.token
                }
            })
            .then(res => {
                console.log(res);
                this.setState({
                    article: {
                        ...this.state.article,
                        ...res.data.data
                    },
                    redirect: '/article/' + res.data.data.slug,
                    loading: true
                })
            })
            .catch(err => {
                this.setState({ loading: true });
                console.log(err.response);
            })
        };
    
    render() {
        const { classes } = this.props;
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        if (this.state.loading) {
            return <Spinner />;
        }
        let approve = null;
        if (this.props.isAdmin) {
                approve = (<FormControlLabel 
                    control={<Checkbox  checked={this.state.status} onChange={this.checkboxHandler} name="approve" />}
                    label="Approve Article"
                    color="primary"
                />)
        }
        return (
            
            <div className={classes.root}>
                <form onSubmit={this.submitHandler}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                id="title"
                                label="Title"
                                variant="outlined"
                                value={this.state.inputs.title.value}
                                error={!this.state.inputs.title.valid}
                                helperText={this.state.inputs.title.valid?null:'Title should be between 20 and 200 characters'}
                                multiline
                                rows={2}
                                onChange={(event) => this.inputChangedHandler(event, 'title')}
                              
                        />
                        </Grid>
                        <Grid item xs={12}>
                             <TextField
                                id="excerpt"
                                label="Excerpt"
                                value={this.state.inputs.excerpt.value}
                                error={!this.state.inputs.excerpt.valid}
                                helperText={this.state.inputs.excerpt.valid?null:'Excerpt should be at least 50 characters'}
                                multiline
                                variant="outlined"
                                rows={2}
                                onChange={(event) => this.inputChangedHandler(event, 'excerpt')}
                                        />
                        </Grid>
                        <Grid item xs={12} className={classes.ckEditor}>
                            <CKEditor
                                editor={ClassicEditor}
                                onInit={editor => {
                                }}
                                onChange={(event, editor) => this.ckeditorInputChangeHandler(event, editor)}
                                onBeforeLoad={(CKEDITOR) => (CKEDITOR.disableAutoInline = true)}
                                data={this.state.inputs.content.value}
                                
                            />
                            {this.state.inputs.content.valid?null:<Alert variant='error'>Content should be at least 100 characters</Alert>}
                        </Grid>
                        <Grid item xs={12} lg={6}>
                            <FormControl className={classes.formControl}>
                                <InputLabel id="demo-mutiple-chip-label">Select Categories</InputLabel>
                                <Select
                                labelId="demo-mutiple-chip-label"
                                id="demo-mutiple-chip"
                                multiple
                                value={this.state.inputs.categories.value}
                                onChange={(event) => this.categoryChangeHandler(event)}
                                input={<Input id="select-multiple-chip" />}
                                renderValue={(selected) => (
                                    <div className={classes.chips}>
                                    {selected.map((value) => (
                                        <Chip color = 'primary'  key={value} label={this.categoryNameSearchHandler(value)} value={value}
                                            className={classes.chip}
                                        />
                                    ))}
                                    </div>
                                )}
                                MenuProps={MenuProps}
                                >
                                {this.props.categories.map((category) => (
                                <MenuItem key={category.title} value={category.identifier} >
                                    {category.title}
                                    </MenuItem>
                                ))}
                                </Select>
                            </FormControl>
                            {this.state.inputs.content.valid?null:<Alert variant='error'>Select at least one category</Alert>}
                        </Grid>
                        <Grid item xs={12} lg={6} className={classes.submit}>
                            <label htmlFor="upload-photo">
                                <input
                                    style={{ display: 'none',margin:'10%' }}
                                    id="upload-photo"
                                    name="upload-photo"
                                    type="file"
                                    onChange={(event) => this.imageChangeHandler(event)}

                                />

                                <Fab
                                    color="primary"
                                    size="small"
                                    component="span"
                                    aria-label="add"
                                    variant="extended"
                                >
                                    <AddIcon /> Upload photo
                                </Fab>
                            </label>
                            {this.state.inputs.content.valid?null:<Alert variant='error'>image can be of maximum size 4mb and of type jpg,jpeg or png</Alert>}
                        </Grid>
                        <Grid item xs={12}  className={classes.submit} >
                            {approve}
                            </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            disabled={!this.state.inputs.title.valid || !this.state.inputs.excerpt.valid||
                                            !this.state.inputs.content.valid || !this.state.inputs.categories.valid 
                                            || !this.state.inputs.image.valid }
                            variant="contained"
                            color="primary"
                            className={classes.submit}>
                            Submit
                        </Button>
                    </Grid> 
                    </form>
                    
            </div>
        );
    }

  
}
const mapStateToProps = (state) => {
    return {
        categories: state.categories.categories,
        isAdmin: state.userDetails.userDetails.isAdmin,
        token: state.auth.token,
        tokenType: state.auth.tokenType
    }
}
export default connect(mapStateToProps)(withStyles(useStyles)(withErrorHandler(UpdateArticle,axios)));