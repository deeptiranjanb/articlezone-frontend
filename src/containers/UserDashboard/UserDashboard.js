import React, { Component } from 'react';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';

import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';



import DataBlock from '../../components/DataBlock/DataBlock';
import TableDisplay from '../../components/TableDisplay/TableDisplay';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom';
import * as actions from '../../store/actions/index';

const useStyles = (theme) => ({
    root: {
        display: 'flex',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
});

class UserDashboard extends Component {
    state = {
        open: true
    }
    
    componentDidMount() {
            this.props.onFetchArticles();
            this.props.onFetchComments();

    }
    articleHandler(article) {
        this.props.history.push('article/' + article.slug);
    }
    closeHandler() {
        this.setState({ open: false });
    }
    render() {
        const { classes } = this.props;
        const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

        let alert = this.props.userDetails.isVerified === 1 ?
            this.state.open ?
            <Alert severity="success" open={this.state.open} onClose={() => { this.closeHandler() }}>
            You have successfully verified your account
            </Alert> : null
            : this.state.open ? <Alert severity="error" open={this.state.open} onClose={() => { this.closeHandler() }}>
                please verify your account
             </Alert > : null;
        if (this.props.userDetails.isAdmin) {
            alert = null;
        }

        const commentsTable = ['article', 'commentContent', 'creationDate'];
        const articlesTable = ['title', 'commentCount', 'creationDate'];

        return (
            <Container maxWidth="lg" className={classes.container}>
                <Box align="right" m={1}>
                    <Button href={'/updateusers/' + this.props.userDetails.id} variant='contained' color="primary">Edit profile</Button>
                    </Box>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        {alert}
                    </Grid>
                     <Grid item xs={12} md={6}>
                        <Paper className={fixedHeightPaper}>
                                <Typography component="h2" variant="h6" color="primary" gutterBottom>
                                My Profile
                                </Typography>
                            <Typography variant='body1' gutterBottom>
                                {"Name: " + this.props.userDetails.name }
                            </Typography>
                            <Typography variant='body1' gutterBottom>
                                {"Email: " + this.props.userDetails.email }
                            </Typography>
                            <Typography variant='body1' gutterBottom>
                                {"Mobie No: " + this.props.userDetails.mobileNo }
                            </Typography>   
                            <Typography variant='body1' gutterBottom>
                                {this.props.userDetails.isAdmin ? "User Type: Admin User" : "User Type: Regular User" }
                                </Typography>
                        </Paper>
                    </Grid>
                     <Grid item xs={12} md={6}>
                        <Paper className={fixedHeightPaper}>
                            <DataBlock
                                title='My Comments'
                                total={this.props.totalComments}
                             />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={6} >
                        <Paper className={fixedHeightPaper}>
                            <DataBlock
                                title='My Articles'
                                total={this.props.totalArticles}
                                />
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        {this.props.articles.length > 0 ?
                            <Paper className={classes.paper}>
                                <TableDisplay
                                    displayDetails={articlesTable}
                                    name='articles'
                                    show = {(article)=>this.articleHandler(article)}
                                    content={this.props.articles} />
                            </Paper> : null}
                    </Grid>
                    <Grid item xs={12}>
                        {this.props.comments.length > 0?
                            <Paper className={classes.paper}>
                                <TableDisplay
                                    displayDetails={commentsTable}
                                    name='comments'
                                    content={this.props.comments} />
                            </Paper> : null}
                    </Grid>
                </Grid>
            </Container>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        articles: state.userArticles.articles,
        totalArticles: state.userArticles.totalArticles,
        comments: state.userComments.comments,
        totalComments: state.userComments.totalComments,
        userDetails: state.userDetails.userDetails
        

    }
    }
const mapDispatchToProps = (dispatch) => {
    return {
        onFetchArticles: () => dispatch(actions.fetchUserArticles()),
        onFetchComments: () => dispatch(actions.fetchUserComments())
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(WithErrorHandler(UserDashboard,axios)));