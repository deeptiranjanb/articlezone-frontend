import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Aux from '../../hoc/Aux/Aux';
import Pagination from '@material-ui/lab/Pagination';
import axios from '../../axios-custom.js';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';



import Article from './Article/Article';
const useStyles = (theme) => ({
     root: {
        flexGrow: 1,
        margin: theme.spacing(1),
        justifyContent: 'center'
     },
    control: {
          padding: theme.spacing(2),
     },
     Pagination: {
         margin: theme.spacing(2),
         justifyContent: 'center'
         
    }
 });

class Articles extends Component {
    state = {
        page: 1,
        redirect: null
    }
    componentDidMount() {
        this.props.onFetch();
        this.setState({ redirect: null });
    }

    fullArticleViewHandler = (slug) => {
        this.setState({ redirect: '/article/' + slug });    
    }
    pageChangeHandler = (event, page) => {
        this.setState({ page: page });
        const url = '/articles?approvalStatus=1&page=' + page;
        console.log(url);
        this.props.onPageChange(url);
    }
    render() {
        if (this.state.redirect) {
            const path = this.state.redirect;

            return <Redirect to={path} />;
        }
        const { classes } = this.props;
        let articles =
            (
                <Grid container className={classes.root} spacing={2} >
                    {Array.from(new Array(9)).map((item, index) => (
                        <Grid item xs={12} lg={8}>
                            <Aux>
                            <Skeleton variant="rect" height={210}/>
                            <Skeleton  height={10} />
                            <Skeleton  height={10} />
                            <Skeleton height={10} />
                            </Aux>
                            </Grid>
                      )
                    )}
                    </Grid>
                );
        if (!this.props.loading) {
            articles = (<Aux>
                <Grid container className={classes.root} spacing={2} >
                {this.props.articles.map((article) => (
                        <Article
                            key={article.identifier}
                            title={article.title}
                            excerpt={article.excerpt}
                            content={article.content}
                            creationDate={article.creationDate}
                            commentCount={article.commentCount}
                            image={article.image}
                            className={classes.paper}
                            slug={article.slug}
                            clicked={(event,slug) => this.fullArticleViewHandler(event,slug)}/>
                ))}
                   
                </Grid>
                <Grid container className={classes.Pagination}>

                    <Pagination count={this.props.totalPages} page={this.state.page} pagesize='large' onChange={this.pageChangeHandler} />

                    
                </Grid>
            </Aux>)
        }
        return (
            <Aux>
                {articles}
                </Aux>
            
            


        );
    }
}
const mapStateToProps = (state) => {
    return {
    articles: state.fetchArticles.articles,
    error: state.fetchArticles.error,
    loading: state.fetchArticles.loading,
    totalPages: state.fetchArticles.totalPages,    
    }
    
}
const mapDispatchToProps = dispatch => {
    return {
        onFetch: () => dispatch(actions.fetchArticles()),
        onPageChange: (url) => dispatch(actions.fetchNextPage(url))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(withErrorHandler(Articles,axios)));
