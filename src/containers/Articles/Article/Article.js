import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';



const useStyles = (theme) => ({

      card: {
        display: 'flex',
      },
      cardDetails: {
        flex: 1,
      },
      cardMedia: {
        width: 160,
      },
});

class Article extends Component {

  
    render() {
        const { classes } = this.props;

      return (
          <Grid item xs={12} lg={8}>
          <CardActionArea component="a" >
            <Card className={classes.card}>
               <Hidden xsDown>
            <CardMedia className={classes.cardMedia}
                        image={"http://articlezone-env-1.eba-7txdzhxi.ap-south-1.elasticbeanstalk.com/image/" + this.props.image}
                        title="article Image" />
          </Hidden>
              <div className={classes.cardDetails}>
                
                <CardContent>
                  <Typography component="h2" variant="h5">  
                     {this.props.title}
                  </Typography> 
                <Typography variant="subtitle1" color="textSecondary">
                    {'On ' + new Date(this.props.creationDate).toISOString().split('T')[0]}
              </Typography>
          <Typography variant="subtitle1" color="textSecondary" paragraph onClick={() => this.props.clicked(this.props.slug)}>
            {this.props.excerpt}
                  </Typography>
                  <Button size="small" color="primary">
          {this.props.commentCount + " Comments"}
        </Button>
              <Button size="small" color="primary" onClick={() => this.props.clicked(this.props.slug)}>
          Read more
        </Button>
                </CardContent>
          </div>
               
           </Card>
        </CardActionArea>
          </Grid>
        
  );
    }
}
export default withStyles(useStyles)(Article);
