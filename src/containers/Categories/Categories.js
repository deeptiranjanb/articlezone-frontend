import React, { Component } from 'react';
import {connect} from 'react-redux';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';


import axios from '../../axios-custom.js';
import Aux from '../../hoc/Aux/Aux';
import Title from '../../components/UI/Title/Title';

class Categories extends Component {
  state = {
    open: false,
    input: {  
      category: {
        title: ''
      },
    },
    action: ''
  }
  openHandler = (event,category,action) => {
    this.setState({
      open: true,
      input: {
        ...this.state.input,
        category: {
          ...this.state.input.category,
          ...category
        }
      },
      action: action
    });
    console.log(this.state);
  }
  closeHandler = () => {
    this.setState({ open: false });
    console.log(this.state);
  }
  inputChangeHandler = (event) => {
    this.setState({
      input: {
        ...this.state.input,
        category: {
          ...this.state.input.category,
          title:event.target.value
        }
        
      }
    });
  }
  submitHandler = () => {
    if (this.state.action === 'add') {
      const url = '/categories';
      let formData = new FormData();
      formData.append('title', this.state.input.category.title);
      axios.post(url, formData, {
        headers: {
          Authorization: this.props.tokenType + ' ' + this.props.token,
          'content-type': 'multipart/form-data'
        }
      })
        .then(response => {
          console.log(response);
        })
        .catch(error => console.log(error.response));
    }
   if (this.state.action === 'update') {
      const url = '/categories/'+this.state.input.category.identifier;
      let formData = new FormData();
     formData.append('title', this.state.input.category.title);
     formData.append('_method', 'put');
      axios.post(url, formData, {
        headers: {
          Authorization: this.props.tokenType+' '+ this.props.token,
          'content-type': 'multipart/form-data'
        }
      })
        .then(response => {
          console.log(response);
          this.setState({
            open: false,
          });
        })
        .catch(error => console.log(error.response));
   }
    if (this.state.action === 'delete') {
        const url = '/categories/' + this.state.input.category.identifier;
        let formData = new FormData();
        formData.append('_method', 'delete');
        axios.post(url, formData, {
                headers: {
                    Authorization: this.props.tokenType + ' ' + this.props.token,
                    'content-type': 'multipart/form-data'
                }
            })
            .then(response => {
              console.log(response);
              this.setState({
                open: false,
              })
            })
          .catch(error => {
            console.log(error.response);
            this.setState({open: false})
          }
          );
    }
    
  }


    render() {
        return (
            <Aux>
        <Title>{this.props.name}</Title>
            <Table size="small">
                      <caption><Button color='primary' variant='contained'onClick={(event)=>this.openHandler(event,null,'add')}>Add New Category</Button></caption>

        <TableHead>
          <TableRow>
            <TableCell>Category Name</TableCell>
            <TableCell>Number of article</TableCell>
            <TableCell />
            <TableCell />
          </TableRow>
        </TableHead>
                    <TableBody>
                        
                        {this.props.content.map((category) => (
                                 <TableRow key={category.identifier}>
                                     <TableCell>{category.title}</TableCell>
                                     <TableCell>{category.articleCount}</TableCell>
                            <TableCell><Button onClick={(event)=>this.openHandler(event,category,'update')}><EditIcon  color='primary' /></Button></TableCell>
                                     <TableCell><Button onClick={(event)=>this.openHandler(event,category,'delete')}><DeleteIcon color='secondary' /></Button></TableCell>
                                 </TableRow>
                        ))}
                        
        </TableBody>
            </Table>
            <Dialog open={this.state.open} onClose={this.closeHandler} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Category</DialogTitle>
        <DialogContent>
          <DialogContentText>
                  {this.state.action}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Title"
            value={this.state.input.category.title}
            type="text"
            onChange ={(event) => this.inputChangeHandler(event)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={this.closeHandler} color="primary">
            Cancel
          </Button>
          <Button onClick={this.submitHandler} color="primary">
                    Confirm
          </Button>
        </DialogActions>
      </Dialog>
                </Aux>
        );
    }
}
const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    tokenType: state.auth.tokenType
  }
}

export default connect(mapStateToProps)(WithErrorHandler(Categories,axios));