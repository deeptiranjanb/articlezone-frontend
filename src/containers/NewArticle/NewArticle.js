import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Alert from '@material-ui/lab/Alert';

import axios from '../../axios-custom.js';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';



const useStyles = (theme) => ({
  root: {
        '& .MuiTextField-root': {
            width: "100%",
         marginTop: "2%",
        marginBottom: "2%",
        },
        margin: "5%",
        
    },
    ckEditor: {
        marginTop: "2%",
        marginBottom: "2%",
        width: "70%"
    },
    submit: { 
          marginTop: "2%",
        marginBottom: "2%",
         width: "100%",
         
    },
    formControl: {
         marginTop: "2%",
        marginBottom: "2%",
        width: "100%",
     }
});


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

class NewArticle extends Component {
     state = {
        inputs: {
            title: {
                value: '',
                validation: {
                    required: true,
                    minLength: 10,
                    maxLength: 100
                },
                valid: true,
                touched: false
            },
            excerpt: {
                value: '',
                validation: {
                    required: true,
                    minLength: 50,
                },
                valid: true,
                touched: false
            },
            content: {
                value: '',
                validation: {
                    required: true,
                    minLength: 20,
                },
                valid: true,
                touched: false
            },
            image: {
                value: null,
                validation: {
                        required: false,
                        mimeType: ['image/jpg','image/jpeg','image/png'],
                        maxSize: 40000000
                    },
                    valid: true,
                touched: false
            },
            categories: {
                value: [],
                validation: {
                        array: true
                    },
                    valid: true,
                touched: false
            },
            author: {
                value: this.props.id,
                touched: false
            }
         },
         loading: false,
         article: null,
         error: null, 
         redirect: null
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (rules.array) {
            isValid = value.length >= 1 && isValid;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.minLength) {
            isValid = value.trim().length >= rules.minLength && isValid;

        }
        if (rules.maxLength) {
            isValid = value.trim().length <= rules.maxLength && isValid;
        }
        if (rules.mimeType) {
            isValid = rules.mimeType.indexOf(value.type) !== -1;

        }
        if (rules.maxSize) {
            isValid = value.size <= rules.maxSize && isValid;
        }
        return isValid;
    }
      inputChangedHandler = (event, name) => {
          const updatedInputs = {
              ...this.state.inputs,
              [name]: {
                  ...this.state.inputs[name],
                  value: event.target.value,
                  valid: this.checkValidity(event.target.value,this.state.inputs[name].validation),
                  touched: true
              },
          };
          this.setState({
              inputs: {
                  ...updatedInputs,
                  author: {
                      ...this.state.inputs.author,
                      value: this.props.id,
                      touched: true
                  }
              }
          })
      }
    ckeditorInputChangeHandler = (event, editor) => {
           const updatedInputs = {
               ...this.state.inputs,
               content: {
                   ...this.state.inputs.content,
                   value: editor.getData(),
                    valid: this.checkValidity(editor.getData(),this.state.inputs.content.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    imageChangeHandler = (event) => {
        const file = event.target.files[0];
        const updatedInputs = {
               ...this.state.inputs,
               image: {
                   ...this.state.inputs.image,
                   value: file,
                    valid: this.checkValidity(file,this.state.inputs.image.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    categoryChangeHandler = (event) => {
        const newCategories = event.target.value;
        const updatedInputs = {
               ...this.state.inputs,
               categories: {
                   ...this.state.inputs.categories,
                   value: newCategories,
                    valid: this.checkValidity(newCategories,this.state.inputs.categories.validation),
                   touched: true
               }
           };
           this.setState({
               inputs: updatedInputs
           })
    }
    chipDeleteHandler = (event) => {
        let newCategories = [...this.state.inputs.categories];
        newCategories = newCategories.splice(event.target.value, 1);
        const updatedInputs = {
            ...this.state.inputs,
            categories: {
                ...this.state.inputs.categories,
                value: newCategories,
                touched: true
            }
        };
        this.setState({
            inputs: updatedInputs
        })
    }
    categoryNameSearchHandler = (id) => {
        let result;
        result = this.props.categories.filter(function (category) {
            return category.identifier === id;
        }); 
        return result[0].title;
    }
    submitHandler = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        let formData = new FormData();
        formData.append('title', this.state.inputs.title.value);
        formData.append('excerpt', this.state.inputs.excerpt.value);
        formData.append('content', this.state.inputs.content.value);
        formData.append('image', this.state.inputs.image.value);
        formData.append('categories', this.state.inputs.categories.value);
        if (this.state.inputs.author.value) {
            formData.append('author', this.state.inputs.author.value);
        }
        const url = '/articles';
        axios.post(url, formData, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
            .then(res => {
                this.setState({
                    ...this.state,
                    success: {
                        ...this.state.success,
                        ...res.data.data
                    },
                    redirect: '/article/' + res.data.data.slug,
                    loading: false
                })
            })
            .catch(err => {
                this.setState({
                    loading: false,
                    error: err.response
                });
                console.log(err.response);
            })
        };
    
    render() {
        const { classes } = this.props;
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        let content = <Spinner />;
        if (!this.state.loading) {
            content = (
                <div className={classes.root}>
                    <form onSubmit={this.submitHandler}>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <TextField
                                    id="title"
                                    label="Title"
                                    variant="outlined"
                                    error={!this.state.inputs.title.valid}
                                    helperText={this.state.inputs.title.valid? null: 'title should be between 20 to 200 characters'}
                                    multiline
                                    rows={2}
                                    required
                                    onChange={(event) => this.inputChangedHandler(event, 'title')}
                              
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="excerpt"
                                    label="Excerpt"
                                    multiline
                                    error={!this.state.inputs.excerpt.valid}
                                    helperText={this.state.inputs.excerpt.valid?null: 'Excerpt should be at least 50 characters'}
                                    variant="outlined"
                                    rows={2}
                                    required
                                    onChange={(event) => this.inputChangedHandler(event, 'excerpt')}
                                />
                            </Grid>
                            <Grid item xs={12} className={classes.ckEditor}>
                                <CKEditor
                                    editor={ClassicEditor}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                    }}
                                    onChange={(event, editor) => this.ckeditorInputChangeHandler(event, editor)}
                                    onBeforeLoad={(CKEDITOR) => (CKEDITOR.disableAutoInline = true)}
                                />
                                {this.state.inputs.content.valid?null: <Alert severity='error'>Content should be at least 100 characters</Alert>}
                            </Grid>
                            <Grid item xs={12} lg={6}>
                                <FormControl className={classes.formControl}>
                                    <InputLabel id="demo-mutiple-chip-label">Select Categories</InputLabel>
                                    <Select
                                        labelId="demo-mutiple-chip-label"
                                        id="demo-mutiple-chip"
                                        multiple
                                        value={this.state.inputs.categories.value}
                                        onChange={(event) => this.categoryChangeHandler(event)}
                                        input={<Input id="select-multiple-chip" />}
                                        renderValue={(selected) => (
                                            <div className={classes.chips}>
                                                {selected.map((value) => (
                                                    <Chip color='primary' key={value} label={this.categoryNameSearchHandler(value)} value={value.identifier}
                                                        className={classes.chip}
                                                    />
                                                ))}
                                            </div>
                                        )}
                                        MenuProps={MenuProps}
                                    >
                                        {this.props.categories.map((category) => (
                                            <MenuItem key={category.identifier} value={category.identifier} >
                                                {category.title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            {this.state.inputs.categories.valid?null: <Alert severity='error'>Select at least one category</Alert>}
                            </Grid>
                            <Grid item xs={12} lg={6} p={6} classes={classes.submit}>
                                <label htmlFor="upload-photo" >
                                    <input
                                        style={{ display: 'none' }}
                                        id="upload-photo"
                                        name="upload-photo"
                                        type="file"
                                        
                                        classes={classes.submit}
                                        onChange={(event) => this.imageChangeHandler(event)}

                                    />

                                    <Fab
                                        color="primary"
                                        size="small"
                                        component="span"
                                        aria-label="add"
                                        variant="extended"
                                        classes={classes.submit}    
                                    >
                                        <AddIcon /> Upload photo
                                </Fab>
                                </label>
                            {this.state.inputs.image.valid?null: <Alert severity='error'>Image can be max of size 4mb and only of type jpg/jpeg/png</Alert>}
                            </Grid>
                        
                            <Button
                                type="submit"
                                width="auto"
                                variant="contained"
                                color="primary"
                                disabled={!this.state.inputs.title.valid || !this.state.inputs.excerpt.valid||
                                            !this.state.inputs.content.valid || !this.state.inputs.categories.valid 
                                            || !this.state.inputs.image.valid }
                                className={classes.submit}>
                                Submit
                        </Button>
                        </Grid>
                    </form>
                    
                </div>
            )
        }

        
        return (content) ;
    }

  
}
const mapStateToProps = (state) => {
    return {
        categories: state.categories.categories,
        id: state.userDetails.userDetails.id
    }
}


export default connect(mapStateToProps)(withStyles(useStyles)(withErrorHandler(NewArticle,axios)));