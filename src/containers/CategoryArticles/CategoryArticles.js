import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { Redirect,withRouter } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Aux from '../../hoc/Aux/Aux';
import Pagination from '@material-ui/lab/Pagination';
import Skeleton from '@material-ui/lab/Skeleton';
import axios from '../../axios-custom.js';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

import Article from '../Articles/Article/Article';
 const useStyles = (theme) =>({
     root: {
         flexGrow: 1,
         justifyContent:'center'
     },
    control: {
          padding: theme.spacing(2),
     },
     Pagination: {
         margin: theme.spacing(2),
         justifyContent: 'center'
         
    }
 });

class CategoryArticles extends Component {
    state = {
        page: 1,
        redirect: false,
        slug: ''
    }
    componentDidMount() {
        const url = "/category/articles/" + this.props.match.params.slug+'?approvalStatus=1';
        console.log(url);
        this.props.onFetch(url);
    }

    fullArticleViewHandler = (slug) => {
        this.setState({ redirect: true, slug: slug });
    
    }
    pageChangeHandler = (event, page) => {
        this.setState({ page: page });
        const url = '/category/articles/' + this.props.match.params.slug + '?approvalStatus=1&page=' + page;
        console.log(url);
        this.props.onPageChange(url);
    }
    render() {
        const { classes } = this.props;
        if (this.state.redirect === true) {
            return <Redirect to={'/article/'+ this.state.slug} />
        }
        let articles =
            (
                <Grid container className={classes.root} spacing={2} >
                    {Array.from(new Array(9)).map((item, index) => (
                        <Grid key={index} item xs={12} lg={8}>
                            <Skeleton variant="rect" height={210}/>
                            <Skeleton  height={10} />
                            <Skeleton  height={10} />
                            <Skeleton  height={10} />
                        </Grid>
                      )
                    )}
                    </Grid>
            );
        if (!this.props.loading) {
            articles=(<Aux>
            <Grid container  className={classes.root} spacing={2} >
                {this.props.articles.map((article) => (
                        <Article
                            key={article.identifier}
                            title={article.title}
                            excerpt={article.excerpt}
                            content={article.content}
                            creationDate={article.creationDate}
                            commentCount={article.commentCount}
                            image={article.image}
                            className={classes.paper}
                            slug={article.slug}
                            clicked={(event,slug) => this.fullArticleViewHandler(event,slug)}/>
                ))}
                   
                </Grid>
                <Grid container className={classes.Pagination}>

                    <Pagination count={this.props.totalPages} page={this.state.page} pagesize='large' onChange={this.pageChangeHandler} />

                    
                </Grid>
                </Aux>)
        }
        return (
            <Aux>
                {articles}
            </Aux>);
    }
}
const mapStateToProps = (state) => {
    return {
    articles: state.fetchArticles.articles,
    error: state.fetchArticles.error,
    loading: state.fetchArticles.loading,
    totalPages: state.fetchArticles.totalPages,    
    }
    
}
const mapDispatchToProps = dispatch => {
    return {
        onFetch: (url) => dispatch(actions.fetchCategoryArticles(url)),
        onPageChange: (url) => dispatch(actions.fetchNextPage(url))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter((withStyles(useStyles)(withErrorHandler(CategoryArticles,axios)))));
