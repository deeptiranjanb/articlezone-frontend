import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import parse from 'html-react-parser';


import Aux from '../../hoc/Aux/Aux';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import Chip from '@material-ui/core/Chip';

import axios from '../../axios-custom';
import Comment from '../../components/Comment/Comment';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';


const useStyles = (theme) => ({
    root: {
        margin: theme.spacing(1),
        wordBreak: 'break-word',

    },
    link: {
        '& > * + *': {
            border: 1
        },
    },
    media: {
        height: "100%"
    },
    catergories: {
        display: 'inline-block',
        border: 1,
        borderShadow: 1
    },
    form: {
        width: "100%",
        display: "inline-block",
        marginBottom: theme.spacing(1)

    }
});


class FullArticle extends Component {
    state = {
        article: {
            id: '',
            title: '',
            excerpt: '',
            content: '',
            creattionDate: '',
            image: ''
        },
        userNames: {
            article: '',
            comments: null
        },
        categories: [],
        comments: [],
        addComment: ''
        }
    
    
    componentDidMount() {
        const url = "/fullarticle/" + this.props.match.params.slug;
        axios.get(url)
            .then(response => {
               this.setState({
                    ...this.state,
                    article: {
                        ...this.state.article,
                        id: response.data.article.identifier,
                        title: response.data.article.title,
                        excerpt: response.data.article.excerpt,
                        content: response.data.article.content,
                        image: response.data.article.image,
                        creationDate: response.data.article.creationDate
                    },
                    categories: response.data.categories
                    ,
                    comments: response.data.comments,
                    userNames: {
                        ...this.state.userNames,
                        article: response.data.userNames.article,
                        comments: response.data.userNames.comment

                    }
                })

            })
            .catch(error => console.log(error))
        
        
    }

    addCommentHandler = (event) => {
        this.setState({
            addComment:event.target.value
        })
    }
    commentSubmitHandler = (event) => {
        event.preventDefault();
        console.log(this.state.addComment);
        if (this.state.addComment) {
            const formData = new FormData();
            formData.append('commentContent', this.state.addComment);
            formData.append('article', this.state.article.id);
            if (this.props.id) {
                formData.append('user',this.props.id);
            }
            axios.post('/comments', formData, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
                .then(response => {
                    console.log(response.data);
                    const newComment = response.data.data;
                    console.log(newComment.user);
                    if (newComment.user !== null ) {
                        this.setState({
                            comments: this.state.comments.concat(newComment),
                          
                        })
                    }
                    this.setState({
                        addComment: ''
                    })
                })
                .catch(error => console.log(error));
        }
    }
    render() {
        const { classes } = this.props;
        let comments = (<Grid item xs={12}>
            <Typography variant="body2">
                No comments yet.Please add some comments
                </Typography>
                </Grid>)
        if (this.state.comments.length !== 0) {
            comments = (this.state.comments.map((comment) => (
                                <Grid key={comment.identifier} item xs={12}>
                                    <Comment
                                        user = {
                                            this.state.userNames.comments[comment.user] ? this.state.userNames.comments[comment.user] : 'Curent User'
                                        }
                                        commentBody={comment.commentContent}
                                        creationDate={comment.creationDate} />
                                    
                                    </Grid>
                            )));
        }
        return (
            <Aux>
                <Container>
                    <Grid container spacing={2} className={classes.root}>
                            <Grid item xs={12}>
                                < Typography variant = "h3"
                                gutterBottom >
                             {this.state.article.title}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                < Typography variant = "subtitle1"
                                gutterBottom >
                            {this.state.article.excerpt}
                                </Typography>
                                
                            </Grid>
                            <Grid item xs={12}>
                                < Typography variant = "subtitle1"
                                gutterBottom >
                                    {<strong>Author: </strong>}{this.state.userNames.article ? this.state.userNames.article: "Guest User"}
                                </Typography>
                                
                            </Grid>
                            <Grid item lg={12}>
                                <CardMedia
                                component="img"
                                alt="article slide"
                                height="100%"
                                    image={"http://articlezone-env-1.eba-7txdzhxi.ap-south-1.elasticbeanstalk.com/image/" + this.state.article.image}
                                title="article image"
                                />
                            </Grid>
                            <Grid item xs={12}>
                            <Box display="flex" flexWrap="wrap" justifyContent="flex-start" m={0} p={0} bgcolor="background.paper">
                                    {this.state.categories.map((category) => (
                                        <Box key={category.identifier} sm={1}>
                                        <Chip label={category.title} component="a" href={"/articles/category/" + category.slug} clickable  />
                                        </Box>
                                    ))}
                            </Box>
                            </Grid>
                            <Grid item xs={12}>
                           <Typography variant="body1" component={'span'} gutterBottom>
                                  {parse(this.state.article.content)}
                            </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="h4" gutterBottom>
                                     < Box fontWeight = "fontWeightBold">
                                        COMMENTS
                                    </Box>
                                </Typography>
                            </Grid>
                            {comments}
                            
                            <Grid item xs={12}>
                                <form   className={classes.form} onSubmit={(event) =>this.commentSubmitHandler(event)}>
                                    <TextField className={classes.form}label="Add a new Comment" variant='outlined' value={this.state.addComment} onChange={this.addCommentHandler}/>
                                    <Button  variant="contained"
                                color="primary" type="submit">Add</Button>
                                </form>
                            </Grid>
                    </Grid>  
                </Container>
            </Aux>
        );
    }
}
const mapStateToProps = state => {
    return {
        id : state.userDetails.userDetails.id 
    }
}

export default connect(mapStateToProps)(withStyles(useStyles)(withErrorHandler(FullArticle,axios)));