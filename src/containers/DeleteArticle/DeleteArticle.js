import React, { Component } from 'react';
import { connect } from 'react-redux';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';
import axios from '../../axios-custom';

class DeleteArticle extends Component {
    componentDidMount() {
        const url = '/articles/' + this.props.match.params.id;
        let formData = new FormData();
        formData.append('_method', 'DELETE');
        axios.post(url, formData, {
            headers: {
                'content-type': 'multipart/form-data',
                Authorization: this.props.tokenType+' '+ this.props.token
            }
        }).then(response => {
            console.log(response);
            this.props.history.goBack();
        }).catch(error => console.log(error.response));
    }
    render() {
        return (<Spinner />);
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        tokenType: state.auth.tokenType
    }
}
const mapDispatchToProps = (dispatch) => {
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(DeleteArticle,axios));
