import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Alert from '@material-ui/lab/Alert';

import ContactMailIcon from '@material-ui/icons/ContactMail';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom.js';

const useStyles = (theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class SignIn extends Component {
  state = {
    inputs: {
      name: {
        value: '',
        touched: false,
         validation: {
                 required: true
             },
             valid: true,
      },
      email: {
        value: '',
         validation: {
           required: true,
           isEmail:true
             },
             valid: true,
           touched: false
          },
      message: {
          value: '',
          validation: {
              required: true
          },
          valid: true,
          touched: false
      }
      },
      show: false
  }
  checkValidity(value, rules) {
      let isValid = true;
      if (rules.required) {
          isValid = value.trim() !== '' && isValid;
      }
      if (rules.isEmail){
          var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
          isValid = regexEmail.test(value.trim()) && isValid;

      }
      return isValid;
  }

  inputChangedHandler = (event, name) => {
    const updatedInputs = {
      ...this.state.inputs,
      [name]: {
        ...this.state.inputs[name],
        value: event.target.value,
        valid:this.checkValidity(event.target.value,this.state.inputs[name].validation),
        touched: true
      }
    };
    this.setState({inputs: updatedInputs})
  }
   submitHandler = (event) => {
       event.preventDefault();
       let formdata = new FormData();
       formdata.append('name', this.state.inputs.name.value);
       formdata.append('email', this.state.inputs.email.value);
       formdata.append('message', this.state.inputs.message.value);
       axios.post('/contactus', formdata)
           .then((response) => {
               console.log(response.data)
               this.setState({
                   inputs: {
                       ...this.state.inputs,
                       name: {
                           ...this.state.inputs.name,
                           value: ''
                       },
                       email: {
                           ...this.state.inputs.email,
                           value: ''
                       },
                       message: {
                           ...this.state.inputs.message,
                           value: ''
                       },
               }});
               this.setState({show: true})
           })
           .catch((error) => console.log(error.response));
       

   }
  

  render() {
    const { classes } = this.props;
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
                    <ContactMailIcon />
            </Avatar>
          <Typography component="h1" variant="h5">
            Contact Form
        </Typography>
          <form className={classes.form} onSubmit={this.submitHandler}>
            <TextField
              variant="outlined"
              margin="normal"
              value={this.state.inputs.name.value}
              type='text'
              error={!this.state.inputs.name.valid}
              helperText={this.state.inputs.name.valid?null:'name is required'}
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              onChange={(event) => this.inputChangedHandler(event,'name')}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              value={this.state.inputs.email.value}
              error={!this.state.inputs.email.valid}
              helperText={this.state.inputs.email.valid?null:'the email is not valid'}
              required
              fullWidth
              name="email"
              label="Email"
              type="email"
              id="email"
              onChange={(event) => this.inputChangedHandler(event,'email')}

                    />
            <TextField
              variant="outlined"
              margin="normal"
              value={this.state.inputs.message.value}
              required
              fullWidth
              error={!this.state.inputs.message.valid}
              helperText={this.state.inputs.message.valid?null: 'message is required'}
              name="message"
              label="Message"
              type="text"
              id="message"
              multiline
              rows={4}
              onChange={(event) => this.inputChangedHandler(event,'message')}
                
            />
            <Button
              type="submit"
              fullWidth
              disabled ={!this.state.inputs.name.valid || !this.state.inputs.email.valid || !this.state.inputs.message.valid}
              variant="contained"
              color="primary"
              className={classes.submit}>
              Submit
          </Button>
                </form>
                {this.state.show?<Alert onClose={() => {this.setState({show: false})}}>Your query has been recorded!</Alert>:null
}
        </div>
      </Container>
    );
  }
}




export default withStyles(useStyles)(withErrorHandler(SignIn, axios));