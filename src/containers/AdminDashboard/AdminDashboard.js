import React, { Component } from 'react';
import { connect } from 'react-redux';
import clsx from 'clsx';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';



import AdminDataBlock from '../../components/AdminDataBlock/AdminDataBlock';
import TableDisplay from '../../components/TableDisplay/TableDisplay';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom';
import Categories from '../Categories/Categories';

const useStyles = (theme) => ({
    root: {
        display: 'flex',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
});

class AdminDashboard extends Component {
    state = {
        articles: {
            values: [],
            approved: 0,
            unapproved: 0,
        },
        comments: {
            values: [],
            approved: 0,
            unapproved: 0
        },
        users: {
            values:[],
            approved: 0,
            unapproved: 0
        },
        categories: {
            values: {},
            total: 0
        },
        show: '',
    }
    
    componentDidMount() { 
        
            const url = '/admin';
            axios.get(url, {
                headers: { Authorization: this.props.tokenType+' '+ this.props.token }
            })
            .then((response) => {
                console.log(response.data);
                this.setState({
                    articles: {
                        ...this.state.articles,
                        approved: response.data.approvedArticles,
                        unapproved: response.data.unapprovedArticles
                    },
                    comments: {
                        ...this.state.comments,
                        approved: response.data.approvedComments,
                        unapproved: response.data.unapprovedComments
                    },
                    users: {
                        ...this.state.comments,
                        approved: response.data.verifiedUsers,
                        unapproved: response.data.unverifiedUsers
                    },
                    categories: {
                        ...this.state.categories,
                        total: response.data.categories
                        
                    },
                })
                console.log(this.state);
            })
            .catch((error) => {
                console.log(error);
            })
        
    }   
    clickHandler = (name) => {
        if (name === 'Articles') {
            const url = '/articles?per_page=21';
            axios.get(url)
                .then(response => {
                    console.log(response.data)
                    this.setState({
                        articles: {
                            ...this.state.articles,
                            values: response.data.data
                        },
                        show: 'articles'
                    })

                })
                .catch(error => console.log(error));
        }
        else if (name === 'Comments') {
            const url = '/comments?per_page=21';
            axios.get(url, {
                headers: { Authorization: this.props.tokenType+' '+ this.props.token }
            })
                .then(response => {
                    console.log(response);
                    this.setState({
                        comments: {
                            ...this.state.comments,
                            values: response.data.data
                        },
                        show: 'comments'
                    })
                })
                .catch(error => console.log(error.response));

        }
        else if (name === 'Users') {
            const url = '/users?per_page=21';
            axios.get(url, {
                headers: { Authorization: this.props.tokenType+' '+ this.props.token }
            })
                .then(response => {
                    this.setState({
                        users: {
                            ...this.state.users,
                            values: response.data.data
                        },
                        show: 'users'
                    })
                    console.log(this.state.users.values);

                })
                .catch(error => console.log(error));
        }
        else if (name === 'Categories') {
            const url = '/categories';
            axios.get(url, {
                headers: { Authorization: this.props.tokenType+' '+ this.props.token }
            })
                .then(response => {
                    console.log(response)
                    this.setState({
                        categories: {
                            ...this.state.categories,
                            values: response.data.data
                        },
                        show: 'Categories'
                    })
                    console.log(this.state.categories.values);

                })
                .catch(error => console.log(error.response));
            
        }
    }
    articleHandler = (article) => {
        console.log(article);
       this.props.history.push('/article/' + article.slug);
    }
    render() {
        const { classes } = this.props;
        const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
        let table = null; 
        switch (this.state.show) {
            case 'articles':
                const articlesDisplay = ['title', 'commentCount', 'approvalStatus', 'creationDate'];
                table = (<Paper className={classes.paper}>
                        <TableDisplay
                    displayDetails={articlesDisplay}
                    name='articles'
                    content={this.state.articles.values}
                    show={(article) => this.articleHandler(article)} /></Paper>);
                break;
            case 'comments':
                const commentsDisplay = ['article', 'commentContent', 'isApproved', 'creationDate'];
                table = (<Paper className={classes.paper}>
                    <TableDisplay
                    displayDetails={commentsDisplay}
                    name='comments'
                    content={this.state.comments.values}
                     /></Paper>);
                break;
            case 'users':
                const usersDisplay = ['name', 'email', 'isVerified', 'isAdmin','creationDate'];
                table = (<Paper className={classes.paper}>
                    <TableDisplay
                    displayDetails={usersDisplay}
                    name='users'
                    content={this.state.users.values} /></Paper>);
                break;
            case 'Categories':
                table = (<Paper className={classes.paper}>
                    <Categories content={this.state.categories.values}></Categories>
                </Paper>);
                break; 
            default:
                table = null;
        }

        return (
            <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3}>
                    <Grid item xs={12} md={6} >
                        <Paper className={fixedHeightPaper}>
                            <AdminDataBlock 
                                title='Users'
                                approved={this.state.users.approved}
                                unapproved={this.state.users.unapproved}
                                clicked={(name) => this.clickHandler(name)}
                                />
                        </Paper>
                    </Grid>
                     <Grid item xs={12} md={6} >
                        <Paper className={fixedHeightPaper}>
                            <AdminDataBlock 
                                title='Articles'
                                approved={this.state.articles.approved}
                                unapproved={this.state.articles.unapproved}
                                clicked={(name) => this.clickHandler(name)}
                                />
                        </Paper>
                    </Grid>
                  
                     <Grid item xs={12} md={6} >
                        <Paper className={fixedHeightPaper}>
                            <AdminDataBlock 
                                title='Comments'
                                approved={this.state.comments.approved}
                                unapproved={this.state.comments.unapproved}
                                clicked={(name) => this.clickHandler(name)}
                                />
                        </Paper>
                    </Grid>
                      <Grid item xs={12} md={6} >
                        <Paper className={fixedHeightPaper}>
                            <AdminDataBlock
                                title='Categories'
                                total={this.state.categories.total}
                                clicked={(name) => this.clickHandler(name)}
                            />
                            
                        </Paper>
                    </Grid>
                    
                    <Grid item xs={12}>
                            {table}
                    </Grid>
                    
                </Grid>
            
            </Container>

        );  
    }
}
const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        tokenType: state.auth.tokenType,
    }
}



export default connect(mapStateToProps)(withStyles(useStyles)(WithErrorHandler(AdminDashboard,axios)));