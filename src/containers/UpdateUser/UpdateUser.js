import React, { Component } from 'react';
import { connect } from 'react-redux';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom';


const useStyles = (theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class UpdateUser extends Component {
    state = {
        inputs: {
            name: {
                value: '',
                touched: false
            },
            email: {
                value: '',
                touched: false
            },
            mobileNo: {
                value: '',
                touched: false
            },
            password: {
                value: '',
                touched: false
            },
            confirmPassword: {
                value: '',
                touched: false
            },
            isAdmin: {
                value: false,
                touched: false
            }
        },
    }

    componentDidMount() {
        const url = '/users/' + this.props.match.params.id;
        axios.get(url, {
            headers:{ Authorization: this.props.tokenType+' '+ this.props.token}
        })
            .then(response => {
                console.log(response.data.data);
                this.setState({
                    inputs: {
                        ...this.state.inputs,
                        name: {
                            ...this.state.inputs.name,
                            value: response.data.data.name
                        },
                        email: {
                            ...this.state.inputs.email,
                            value: response.data.data.email
                        },
                        mobileNo: {
                            ...this.state.inputs.mobileNo,
                            value: response.data.data.mobileNo
                        },
                        isAdmin: {
                            ...this.state.inputs.isAdmin,
                            value: response.data.data.isAdmin
                        }
                    }

                })
                console.log(this.state);
        }).catch(error => console.log(error))

    }

      inputChangedHandler = (event, name) => {
          const updatedInputs = {
              ...this.state.inputs,
              [name]: {
                  ...this.state.inputs[name],
                  value: event.target.value,
                  touched: true
              }
          };
          this.setState({
              inputs: updatedInputs
          })
      }
    checkboxHandler = (event) => {
        this.setState({
            inputs: {
                ...this.state.inputs,
                isAdmin: {
                    ...this.state.inputs.isAdmin,
                    value: event.target.checked,
                    touched: true
                }
            }
        })
    }
    submitHandler = (event) => {
        event.preventDefault();
        let formData = new FormData();
        if (this.state.inputs.name.touched) {
            formData.append('name', this.state.inputs.name.value);
        }
        if (this.state.inputs.email.touched) {
            formData.append('email', this.state.inputs.email.value);
        }
        if (this.state.inputs.mobileNo.touched) {
            formData.append('mobileNo', this.state.inputs.mobileNo.value);
        }
        if (this.state.inputs.password.touched) {
            formData.append('password', this.state.inputs.password.value);
        }
        if (this.state.inputs.confirmPassword.touched) {
            formData.append('password_confirmation', this.state.inputs.confirmPassword.value);
        }
        if (this.state.inputs.isAdmin.value) {
            formData.append('isAdmin', 'true');
        }
        formData.append('_method', 'put');

        const url = '/users/' + this.props.match.params.id;
        axios.post(url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: this.props.tokenType+' '+ this.props.token
            }
        }).then(response => {
            console.log(response);
            this.props.history.goBack();
        })
            .catch(error => console.log(error.response));
        
    }
    render() {
        const { classes } = this.props;
        return (
            <Container component="main" maxWidth='xs'>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Update User
        </Typography>
                    <form className={classes.form} onSubmit={this.submitHandler}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    name="name"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="name"
                                    label="name"
                                    value= {this.state.inputs.name.value}
                                    onChange={(event) => this.inputChangedHandler(event, 'name')}
                                />
                            </Grid>
                          
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    name="email"
                                    value={this.state.inputs.email.value}
                                    onChange={(event) => this.inputChangedHandler(event, 'email')}
                                />
                            </Grid>
                            
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="mobileNo"
                                    label="Mobile No"
                                    name="mobileNo"
                                    value={this.state.inputs.mobileNo.value}
                                    onChange={(event) => this.inputChangedHandler(event, 'mobileNo')}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    name="password"
                                    label="New password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    onChange={(event) => this.inputChangedHandler(event, 'password')}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    name="confirmPassword"
                                    label="Confirm New Password"
                                    type="password"
                                    id="confirmPassword"
                                    autoComplete="current-password"
                                    onChange={(event) => this.inputChangedHandler(event, 'confirmPassword')}
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                    control={<Checkbox checked={this.state.inputs.isAdmin.value} onChange={this.checkboxHandler} name="approve" />}
                    label="Admin User"
                    color="primary"
                />
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            maxwidth='xs'
                            className={classes.submit}>
                            Update
                        </Button>
                    </form>
                </div>
            
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        token: state.auth.token,
        tokenType: state.auth.tokenType,
        isAdmin: state.userDetails.userDetails.isAdmin,

    }
};


export default connect(mapStateToProps)(withStyles(useStyles)(withErrorHandler(UpdateUser,axios)));