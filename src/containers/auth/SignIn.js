import React, { Component } from 'react';
import { connect } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Alert from '@material-ui/lab/Alert';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import Spinner from "../../components/UI/Spinner/Spinner";
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom.js';
import * as actions from '../../store/actions/index';

const useStyles = (theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class SignIn extends Component {
  state = {
    inputs: {
      email: {
        value: '',
        touched: false,
         validation: {
                 required: true,
                 isEmail: true
             },
             valid: true,
      },
      password: {
        value: '',
         validation: {
                 required: true
             },
             valid: true,
           touched: false
      }
    },
  }
  checkValidity(value, rules) {
      let isValid = true;
      if (rules.required) {
          isValid = value.trim() !== '' && isValid;
      }
      if (rules.minLength) {
          isValid = value.trim().length >= rules.minLength && isValid;
      }
      if (rules.isEmail){
          var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
          isValid = regexEmail.test(value.trim()) && isValid;

      }
      if (rules.maxLength) {
          isValid = value.trim().length <= rules.maxLength && isValid;
      }
      return isValid;
  }

  inputChangedHandler = (event, name) => {
    const updatedInputs = {
      ...this.state.inputs,
      [name]: {
        ...this.state.inputs[name],
        value: event.target.value,
        valid:this.checkValidity(event.target.value,this.state.inputs[name].validation),
        touched: true
      }
    };
    this.setState({inputs: updatedInputs})
  }
   submitHandler = (event) => {
       event.preventDefault();
       this.props.onAuth(this.state.inputs.email.value, this.state.inputs.password.value);
   }
  

  render() {
    const { classes } = this.props;
    let error = null;
    if (this.props.error) {
      error = <Alert severity="error"> Email or password is wrong </Alert >;
    }
    if (this.props.loading) {
      return <Spinner />;
    }
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
        </Typography>
          <form className={classes.form} onSubmit={this.submitHandler}>
            <TextField
              variant="outlined"
              margin="normal"
              error={!this.state.inputs.email.valid}
              helperText={this.state.inputs.email.valid? null:'email is Invalid'}
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              onChange={(event) => this.inputChangedHandler(event,'email')}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              error={!this.state.inputs.password.valid}
              helperText={this.state.inputs.password.valid?null:'password is not valid'}
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(event) => this.inputChangedHandler(event,'password')}

            />
            {error}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              disabled={!this.state.inputs.email.valid || !this.state.inputs.password.valid}
              color="primary"
              className={classes.submit}>
              Sign In
          </Button>
            <Grid container>
              {/*<Grid item xs>
                <Link href="http://localhost:8000/password/reset" variant="body2">
                  Forgot password?
              </Link>
              </Grid> */}
              <Grid item>
                <Link href="/signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    loading: state.auth.loading,
    isAuthenticated: state.auth.token !== null,
    error: state.auth.error
  }
}

const mapDispatchToProps = dispatch => {
  return { 
    onAuth: (email, password) => dispatch(actions.auth(email,password))
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(WithErrorHandler(SignIn, axios)));