import React, { Component } from 'react';
import { connect } from 'react-redux';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-custom.js';
import Spinner from '../../components/UI/Spinner/Spinner';

import * as actions from '../../store/actions/index';



const useStyles = (theme) => ({
    root: {
        maxWidth: 200
    },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '80%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class SignUp extends Component {
    state = {
        inputs: {
            firstName: {
                value: '',
                validation: {
                 required: true
                },
                valid: true,
                touched: false
            },
            lastName: {
                value: '',
                validation: {
                 required: true,
                },
                valid: true,
                touched: false
            },
            email: {
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: true,
                touched: false
            },
            mobileNo: {
                value: '',
                validation: {
                    required: true,
                    mobile: true
                },
                valid: true,
                touched: false
            },
            password: {
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: true,
                touched: false
            },
            confirmPassword: {
                value: '',
                validation: {
                    required: true,
                    minLength: 6,
                    confirm: true
                },
                valid: true,
                touched: false
            }
        },
    }

    checkValidity(value, rules) {
      let isValid = true;
      if (rules.required) {
          isValid = value.trim() !== '' && isValid;
      }
      if (rules.minLength) {
          isValid = value.trim().length >= rules.minLength && isValid;
      }
      if (rules.maxLength) {
          isValid = value.trim().length <= rules.maxLength && isValid;
      }
      if (rules.isEmail){
          var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
          isValid = regexEmail.test(value.trim()) && isValid;

      }
      if (rules.mobile) {
         var regexMobile = /^([+]\d{2}[ ])?\d{10}$/;
         isValid = regexMobile.test(value.trim()) && isValid;
      }
        if (rules.confirm) {
            isValid = value === this.state.inputs.password.value && isValid;
      }
      return isValid;
  }

      inputChangedHandler = (event, name) => {
          const updatedInputs = {
              ...this.state.inputs,
              [name]: {
                  ...this.state.inputs[name],
                  value: event.target.value,
                  valid:this.checkValidity(event.target.value,this.state.inputs[name].validation),
                  touched: true
              }
          };
          this.setState({
              inputs: updatedInputs
          })
      }
    submitHandler = (event) => {
        event.preventDefault();
        this.props.onSignup(this.state.inputs);
    }
    render() {
        const { classes } = this.props;
        if (this.props.loading) {
            return <Spinner />;
        }
        return (
            <Container component="main" className={classes.container}>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
        </Typography>
                    <form className={classes.form} onSubmit={this.submitHandler}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    error={!this.state.inputs.firstName.valid}
                                    helperText={this.state.inputs.firstName.valid?null: 'first name is required'}
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    onChange={(event) => this.inputChangedHandler(event, 'firstName')}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        error={!this.state.inputs.lastName.valid}
                                        helperText={this.state.inputs.lastName.valid?null: 'Last name is required'}
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="lname"
                                        onChange={(event) => this.inputChangedHandler(event, 'lastName')}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    error={!this.state.inputs.email.valid}
                                    helperText={this.state.inputs.email.valid?null: 'Email is invalid'}
                                    label="Email Address"
                                    name="email"
                                    type="email"
                                    onChange={(event) => this.inputChangedHandler(event, 'email')}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    error={!this.state.inputs.mobileNo.valid}
                                    helperText={this.state.inputs.mobileNo.valid?null: 'Mobile Number is invalid'}
                                    id="mobileNo"
                                    label="Mobile No"
                                    name="mobileNo"
                                    onChange={(event) => this.inputChangedHandler(event, 'mobileNo')}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    error={!this.state.inputs.password.valid}
                                    name="password"
                                    label="Password"
                                    type="password"
                                    helperText={this.state.inputs.password.valid?null: 'password must be of at least 6 characters'}
                                    id="password"
                                    autoComplete="current-password"
                                    onChange={(event) => this.inputChangedHandler(event, 'password')}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    error={!this.state.inputs.confirmPassword.valid}
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    helperText={this.state.inputs.confirmPassword.valid?null: 'password does not match'}
                                    type="password"
                                    id="confirmPassword"
                                    autoComplete="current-password"
                                    onChange={(event) => this.inputChangedHandler(event, 'confirmPassword')}
                                />
                            </Grid>
                            {/* <Grid item xs={6}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid> */}
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            disabled={!this.state.inputs.firstName.valid || !this.state.inputs.lastName.valid
                                || !this.state.inputs.email.valid || !this.state.inputs.mobileNo.valid
                                || !this.state.inputs.password.valid || !this.state.inputs.confirmPassword.valid}
                            variant="contained"
                            color="primary"
                            maxWidth='xs'
                            className={classes.submit}>
                            Sign Up
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="/signin" variant="body2">
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            
            </Container>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        name: state.signup.name,
        loading: state.signup.loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSignup: (data) => dispatch(actions.signup(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(WithErrorHandler(SignUp,axios)));