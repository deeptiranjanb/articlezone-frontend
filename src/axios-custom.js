import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://articlezone-env-1.eba-7txdzhxi.ap-south-1.elasticbeanstalk.com'
});

//instance.defaults.headers.common['Authorization'] = "INSTANCE AUTH TOKEN";

export default instance;