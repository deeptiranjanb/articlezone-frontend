import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import authReducer from './store/reducers/auth';
import fetchArticlesReducer from './store/reducers/fetchArticles';
import signupReducer from './store/reducers/signup';
import userDetailsReducer from './store/reducers/fetchUserDetails';
import userArticlesReducer from './store/reducers/fetchUserArticles';
import userCommentsReducer from './store/reducers/fetchUserComments';
import categoriesReducer from './store/reducers/fetchCategories';



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    auth: authReducer,
    fetchArticles: fetchArticlesReducer,
    signup: signupReducer,
    userDetails: userDetailsReducer,
    userArticles: userArticlesReducer,
    userComments: userCommentsReducer,
    categories: categoriesReducer
});
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

const app = (
    <Provider store={store}>
    <BrowserRouter>
        <App />
    </BrowserRouter>
    </Provider>
    
);


ReactDOM.render( app, document.getElementById( 'root' ) );
registerServiceWorker();
