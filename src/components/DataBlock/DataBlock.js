import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from '../UI/Title/Title';


const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

const DataBlock = (props) => {
  const classes = useStyles();
  return (
    <React.Fragment>
          <Title>{props.title}</Title>
      <Typography component="p" variant="h4">
          {props.total}
          </Typography>
          <Typography color="textSecondary" className={classes.depositContext} m={1}>
             {"On " + new Date().toISOString().split('T')[0]}
      </Typography>
    </React.Fragment>
  );
 }

export default DataBlock;