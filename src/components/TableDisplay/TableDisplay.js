import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../UI/Title/Title';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import parse from 'html-react-parser';







const userArticles = (props) => {
  return (
    <React.Fragment>
      <Title>{props.name}</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            {props.displayDetails.map((item) => {
              return <TableCell key={item}>{item.toUpperCase()}</TableCell>;
            })}
            <TableCell />
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {props.content.map((row) => (
            <TableRow key={row.identifier}>
              {props.displayDetails.map((item) => {
                if (item === 'creationDate') {
                 return <TableCell key={item}>{new Date(row[item]).toISOString().split('T')[0]}</TableCell>
               }
                return <TableCell onClick={props.show?()=>props.show(row):null} key={item}>{parse(row[item].toString())}</TableCell>
              })}
              <TableCell><Button href={'/update'+props.name+'/'+row.identifier}><EditIcon  color='primary' /></Button></TableCell>
              <TableCell><Button href={'/delete'+props.name+'/'+row.identifier}><DeleteIcon color='secondary' /></Button></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </React.Fragment>
  );
}
export default userArticles;