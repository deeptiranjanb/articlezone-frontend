
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Aux from '../../hoc/Aux/Aux';
import Box from '@material-ui/core/Box';
import parse from 'html-react-parser';





const Comment = (props) => {

    return (
        <Aux>
        <Box color="primary">
        <Typography variant="body1" gutterBottom>
            {parse(props.commentBody)}
                </Typography>
             
        <Typography variant="subtitle2" display="block" gutterBottom>
                {"By "+props.user}
                </Typography>
        <Typography variant="caption" display="block" gutterBottom>
                    {'On ' + new Date(props.creationDate).toISOString().split('T')[0]}
                </Typography>
    
        </Box>
        </Aux>
     );

};

export default Comment;