import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from '../UI/Title/Title';


const useStyles = makeStyles((theme) => ({
    depositContext: {
        flex: 1,
    },
    typography: {
        margin: theme.spacing(1)
    }
}));

const AdminDataBlock = (props) => {
  const classes = useStyles();
  return (
    <React.Fragment>
          <Title>{props.title}</Title>
      <Typography component="p" variant="body1" className={classes.typography}>
          {props.approved?'Approved '+props.title+': '+props.approved : null}
          </Typography>
          <Typography component="p" variant="body1" className={classes.typography}>
            {props.unapproved?'Unapproved '+props.title+': '+props.unapproved:null}
      </Typography>
      <Typography component="p" variant="body1" className={classes.typography}>
            {props.total?'Total: '+props.total:null}
          </Typography>
          <Typography color="textSecondary" className={classes.depositContext} m={1}>
             {"On " + new Date().toISOString().split('T')[0]}
      </Typography>
      <div>
        <Link color="primary"  onClick={() => props.clicked(props.title)}>
          View details
        </Link>
      </div>
    </React.Fragment>
  );
 }

export default AdminDataBlock;